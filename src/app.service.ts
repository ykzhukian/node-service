import { Injectable } from '@nestjs/common';
import axios from 'axios';
@Injectable()
export class AppService {
  async getHello(): Promise<string> {
    const { data } = await axios.get('http://172.17.14.37/');
    console.log('data', data);
    return JSON.stringify(data);
  }
}
