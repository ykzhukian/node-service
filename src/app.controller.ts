import { AppService } from './app.service';
import { Response, Request } from 'express';
import { Controller, Get, Query, Res, Req } from '@nestjs/common';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/')
  async index(@Query() query, @Res() res: Response, @Req() req: Request) {
    // const data = await this.appService.getHello();
    const data = 321;
    return res.render('index', { data });
  }
}
